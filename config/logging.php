<?php

/**
 * 配置日志(支持日期时间格式转换)
 * days         inter   保留日志天数
 */
return [
    'stack' => [
        // 'file'          => APP_HOME . '/logs/nova-{Ymd}.log',
        'file'          => APP_HOME . '/logs/nova.log',
        'days'          => 30
    ]
];
