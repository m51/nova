<?php

return [
    'default' => [
        'host' => [],
        'port' => env('ES_PORT', '9200'),
    ]
];
