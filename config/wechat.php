<?php

return [
    'Host'      => 'https://api.weixin.qq.com',
    // 应用信息
    'AppID'     => env('WECHAT_APPID'),
    'Secret'    => env('WECHAT_SECRET'),
];