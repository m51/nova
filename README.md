# NOVA API FrameWork
NOVA 是一个简单高效 PHP的API框架，追求极速的性能，也最求优雅的开发，简单且高效。可在PHP-FPM或SWOOLE中运行。

## Run In Swoole
```sh
php bin/server.php
```

## 依赖关系
 - PHP >= 8.1
 - Swoole >= 5.0 (使用 swoole 运行)

## 开始使用
```sh
# 初始化仓库
git init
# 获取最新框架代码
git remote add nova git@gitee.com:m51/nova.git
git pull nova master
# 安装依赖包
composer install
```

## 使用 swoole 方式运行
> php bin/server.php

## 关于
