<?php

/**
 * NOVA HOME
 */
define('APP_HOME', dirname(__DIR__));

/**
 * APP RUN
 */
$app = require_once __DIR__.'/../src/bootstrap/app.php';

$app->run();
