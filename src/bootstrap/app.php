<?php

// 加载常量定义
require_once(dirname(__DIR__) . '/bootstrap/define.php');

// 加载自定义函数
require_once(dirname(__DIR__) . '/bootstrap/functions.php');

// Register The Auto Loader
require __DIR__.'/../../vendor/autoload.php';

define('APP_BASE_PATH', dirname(__DIR__));

// new app
$app = new Core\Application\App([
    // API接口模块
    'api' => [
        'route' => require __DIR__ . '/../app/Api/Route.php'
    ],

    // Admin接口模块
    'admin' => [
        'route' => require __DIR__ . '/../app/Admin/Route.php'
    ]
]);

return $app;
