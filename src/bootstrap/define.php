<?php

// define('HASHID_LEN', 16);
// define('PAYMENT_SIGNATURE', 'YRLO5PHZ22ABSFXX');

//  会话标识, 用于redis Key
define('DXE_SESSION', 'session');

// HEADER 协议头
define('APP_XATOKEN',       'xa-token');    // 会话登陆令牌标识
define('APP_XAUID',         'xa-uid');      // 测试账户免登陆协议头
define('APP_XAT',           'xa-t');        // 请求随机数
define('APP_SIGN',          'xa-s');        // 请求签名
define('APP_SLAT',          'XwTSwIm+RzHKrWQoX');
define('APP_SECRET',        'NXYSJC6LWDMZOF25');

// 小蜜蜂存储驱动器以及划分
// define('XMF_OSSDISK',       'xmf');
// define('XMF_OSS_BUCKET',    'sk-xmf');
// define('XMF_OSS_ASSETS',    'assets');      // ASSETS 资源目录, 手动维护
// define('XMF_OSS_I2',        'i2');          // 通用文件存储
// define('XMF_OSS_TEST',      'test');        // 测试目录
