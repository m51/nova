<?php

if (! function_exists('env')) {
    /**
     * Gets the value of an environment variable.
     *
     * @param  string  $key
     * @param  mixed  $default
     * @return mixed
     */
    function env($key, $default = null)
    {
        if (isset($_ENV[$key])) {
            return $_ENV[$key];
        }

        return $default;
    }
}

/**
 * 随机字符串
 */
if (! function_exists('str_random')) {
    function str_random(int $len)
    {
        return bin2hex(random_bytes($len));
    }
}
