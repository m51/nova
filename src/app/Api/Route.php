<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

use App\Api\Controller\CartController;
use App\Api\Controller\CityController;
use App\Api\Controller\DevController;
use App\Api\Controller\GoodsController;
use App\Api\Controller\Security\AuthController;
use App\Api\Controller\ShopController;
use App\Api\Controller\UserController;
use Core\Route\RouteCollector;

return function (RouteCollector $r) {
    $r->middleware(['api'])->group('/api', function ($r) {

        // 运维接口
        $r->get('/dev/halo', [DevController::class, 'halo']);                                           // 服务监测

        // es 测试
        $r->get('/dev/book_search', [DevController::class, 'bookSearch']);                              // es搜索
        $r->get('/dev/book_detail', [DevController::class, 'bookDetail']);                              // es详细

        // 通用接口
        $r->post('/login', [AuthController::class, 'login']);                                           // 账号密码登录
        $r->post('/wxlogin', [AuthController::class, 'loginWechat']);                                   // 用户登录
        $r->get('/city/cascade', [CityController::class, 'cascade']);                                   // 城市级联接口

        /*************************** 需要登录 ***************************/
        $r->middleware(['auth'], function ($r) {
            // 用户
            $r->get('/user/list', [UserController::class, 'list']);                                     // 用户列表
            $r->post('/user/update/{userId:\d+}', [UserController::class, 'update']);                   // 更新用户信息

            // 店铺
            $r->get('/shop/list', [ShopController::class, 'list']);                                     // 店铺列表
            $r->post('/shop/add', [ShopController::class, 'add']);                                      // 新建店铺
            $r->post('/shop/update/{shopId:\d+}', [ShopController::class, 'update']);                   // 修改店铺属性
            $r->get('/shop/detail/{shopId:\d+}', [ShopController::class, 'detail']);                    // 店铺详情

            // 商品
            $r->get('/goods/list', [GoodsController::class, 'list']);                                   // 商品列表
            $r->get('/goods/list_by_group', [GoodsController::class, 'listByGroup']);                   // 商品分组列表
            $r->post('/goods/add', [GoodsController::class, 'add']);                                    // 添加商品
            $r->post('/goods/update/{goodsId:\d+}', [GoodsController::class, 'update']);                // 修改商品属性
            $r->delete('/goods/delete/{goodsId:\d+}', [GoodsController::class, 'delete']);              // 删除商品

            // 商品分组
            $r->get('/goods/group/list', [GoodsController::class, 'groupList']);                        // 商品分组列表
            $r->get('/goods/group/add', [GoodsController::class, 'groupAdd']);                          // 商品分组新建
            $r->get('/goods/group/update/{groupId:\d+}', [GoodsController::class, 'groupAdd']);         // 商品分组修改
            $r->delete('/goods/group/delete/{groupId:\d+}', [GoodsController::class, 'groupAdd']);      // 商品分组删除

            // 购物车
            $r->post('/cart/inc', [CartController::class, 'inc']);                                      // 购买商品数量+1
            $r->post('/cart/dec', [CartController::class, 'dec']);                                      // 购买商品数量-1
            $r->post('/cart/set', [CartController::class, 'set']);                                      // 设置购买商品数量
            $r->post('/cart/checked', [CartController::class, 'checked']);                              // 选中/取消商品
            $r->post('/cart/remove', [CartController::class, 'remove']);                                // 从购物车移除商品
            $r->post('/cart/empty', [CartController::class, 'empty']);                                  // 清空购物车
            $r->get('/cart/list', [CartController::class, 'list']);                                     // 获取购物车商品

            // 订单
            $r->post('/order/add', [OrderController::class, 'add']);                                    // 创建订单
            $r->get('/order/list', [OrderController::class, 'list']);                                   // 订单列表
            $r->delete('/order/delete', [OrderController::class, 'delete']);                            // 删除订单

            // 支付
            $r->post('/pay/way', [PayController::class, 'way']);                                        // 付款方式列表
            $r->post('/pay/launch', [PayController::class, 'launch']);                                  // 发起支付
            $r->post('/pay/receipt', [PayController::class, 'receipt']);                                // 支付票据
        });

        // $r->post('/set/passwd', [UserController::class, 'setPassword']);             // 设置密码(不安全关闭)
    });
};
