<?php

namespace App\Api\Controller;

use Core\Application\Controller;
use G\City\CityService;

class CityController extends Controller
{
    protected $cityService;

    public function __construct()
    {
        parent::__construct();

        $this->cityService = CityService::getInstance();
    }

    public function add()
    {
        # code...
    }

    public function cascade()
    {
        $cascade = $this->cityService->getCascade();

        return $this->success($cascade);
    }

    public function update()
    {
        # code...
    }

    public function detail()
    {
        # code...
    }
}
