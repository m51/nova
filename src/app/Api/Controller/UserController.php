<?php

namespace App\Api\Controller;

use Core\Application\Controller;
use G\User\UserService;

class UserController extends Controller
{
    protected $userService;

    public function __construct()
    {
        parent::__construct();

        $this->userService = UserService::getInstance();
    }

    /**
     * 创建用户
     */
    public function add()
    {
        // $input = $this->getInputJson();

        // $user = $this->userService->add($input);
        return $this->success();
    }

    public function list()
    {
        $paginate = $this->userService->list();

        return $this->success($paginate);
    }

    /**
     * 更新用户信息
     */
    public function update($userId)
    {
        $input = $this->getInputJson();

        $this->userService->update($userId, $input);
        return $this->success();
    }

    /**
     * 设置密码
     */
    public function setPassword()
    {
        $password = '123000';

        $this->userService->setPassword(9527, $password);

        return $this->success();
    }
}
