<?php

namespace App\Api\Controller;

use App\Bundle\Shop\ShopService;
use Core\Application\Controller;
use Core\Support\Validate;

class ShopController extends Controller
{
    protected $shopService;

    public function __construct()
    {
        parent::__construct();
        $this->shopService = ShopService::getInstance();
    }

    // 新建店铺
    #[Validate('name', '名称不能为空')]
    #[Validate('city_id', '请选择所在城市')]
    #[Validate('address', '请填写详细地址')]
    #[Validate('contact_name', '请填写联系人名称')]
    #[Validate('contact_phone', '请填写联系人电话')]
    public function add()
    {
        $input = $this->getInputJson();

        $shop = $this->shopService->add($input);
        return $this->success($shop);
    }

    public function list()
    {
        # code...
    }

    // 更新数据
    public function update($shopId)
    {
        $input = $this->getInputJson();

        $this->shopService->update($shopId, $input);
        return $this->success();
    }

    public function detail($shopId)
    {
        $detail = $this->shopService->detail($shopId);
        return $this->success($detail);
    }
}
