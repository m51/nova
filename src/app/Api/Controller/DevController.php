<?php

namespace App\Api\Controller;

use Core\Application\Controller;
use Core\Elasticsearch\EsFactory;
use Core\Support\Arr;
use Core\Support\Str;
use Core\Support\Validate;

class DevController extends Controller
{

    protected $client;

    public function halo()
    {
        $uuid = Str::uuid4()->getHex();

        return $this->success([
            'uuid' => $uuid,
            's' => str_random(16),
            't' => time(),
        ]);
    }

    public function bookSearch()
    {
        $input = $this->getInputAll();

        $response = $this->getClient()->search('book', [
            "from" => 0,
            "size" => 20,
            'query' => [
                'multi_match' => [
                    'query' => Arr::get($input, 'key'),
                    'fields' => ['name', 'introduce']
                ]
            ],
            // 高亮
            "highlight" => [
                "pre_tags" => "<b class=\"hight\">",
                "post_tags" =>  "</b>",
                "fields" =>  [
                    "name" => new \stdClass(),
                ],
            ],
        ]);

        return $this->success([
            'total' => $response['hits']['total']['value'],
            'data' => array_column($response['hits']['hits'], "_source"),
        ]);
    }

    // 详情
    #[Validate('id', '缺少参数 id')]
    public function bookDetail()
    {
        $input = $this->getInputAll();

        $response = $this->getClient()->get('book', Arr::get($input, 'id'));
        // if ($response) {
        //     return $response['_source'];
        // }

        return $this->success($response ? $response['_source'] : null);
    }

    /**
     * get client
     */
    protected function getClient()
    {
        $client = EsFactory::newClient();

        $this->client = $client;

        return $this;
    }

    /**
     * 搜索
     */
    public function search($index, $body)
    {
        $response = $this->client->search([
            'index' => $index,
            'body'  => $body
        ]);

        return $response->asArray();
    }

    /**
     * 创建数据
     * @param string    index
     * @param mixed     id          主键ID
     * @param array     body        索引数据
     */
    public function index(string $index, mixed $id, array $body)
    {
        return $this->client->index([
            'index' => $index,
            'id'    => $id,
            'body'  => $body
        ]);
    }

    /**
     * 更新数据
     * @param string    index
     * @param mixed     id          主键ID
     * @param array     body        索引数据
     */
    public function update(string $index, mixed $id, array $body)
    {
        return $this->client->update([
            'index' => $index,
            'id'    => $id,
            'body'  => $body
        ]);
    }

    /**
     * 获取内容
     */
    public function get(string $index, $id)
    {
        try {
            $response = $this->client->get([
                'index' => $index,
                'id'    => $id,
            ]);

            return $response->asArray();
        } catch (\Exception $e) {
            return null;
        }
    }

    /**
     * 删除数据
     */
    public function delete($index, $id)
    {
        return $this->client->delete([
            'index' => $index,
            'id' => $id
        ]);
    }
}
