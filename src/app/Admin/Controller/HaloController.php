<?php

namespace App\Admin\Controller;

use Core\Application\Controller;

class HaloController extends Controller
{
    /**
     * halo_demo
     */
    public function actionDemo($id = 0)
    {
        $msg = "[ADMIN] Halo Demo ({$id})!";

        return $this->success([
            'str' => $msg,
        ]);
    }
}
