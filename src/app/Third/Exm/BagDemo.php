<?php

namespace App\Third\Exm;

use Core\Http\Client\BagFoundation;
use Core\Http\Client\Correlation\BagJson;

class BagDemo extends BagFoundation
{
    use BagJson;

    public function url()
    {
        return 'http://test.quickly-fill.campusapp.com.cn/api/common/dict/dimlist';
    }

    public function method()
    {
        return self::METHOD_GET;
    }
}
