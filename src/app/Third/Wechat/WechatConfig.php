<?php

namespace App\Third\Wechat;

class WechatConfig
{
    protected static $host;
    protected static $appId;
    protected static $appSecret;

    public static function getHost()
    {
        return self::$host;
    }

    public static function getAppId()
    {
        return self::$appId;
    }

    public static function getAppSecret()
    {
        return self::$appSecret;
    }

    public static function setHost($val)
    {
        self::$host = $val;
    }

    public static function setAppId($val)
    {
        self::$appId = $val;
    }

    public static function setAppSecret($val)
    {
        self::$appSecret = $val;
    }
}
