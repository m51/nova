<?php

namespace App\Third\Wechat\Bag\Auth;

use App\Third\Wechat\WechatConfig;
use Core\Http\Client\BagFoundation;

/**
 * 登录凭证校验。通过 wx.login 接口获得临时登录凭证 code 后传到开发者服务器调用此接口完成登录流程。
 *
 * [参考文档]https://developers.weixin.qq.com/miniprogram/dev/dev_wxwork/dev-doc/qywx-api/login/code2session.html
 * 返回原始数据
 */
class Code2SessionBag extends BagFoundation
{
    protected $code;

    public function url()
    {
        return WechatConfig::getHost() . '/sns/jscode2session';
    }

    public function method()
    {
        return self::METHOD_GET;
    }

    public function query()
    {
        return [
            'appid'         => WechatConfig::getAppId(),
            'secret'        => WechatConfig::getAppSecret(),
            'js_code'       => $this->code,
            'grant_type'    => 'authorization_code'
        ];
    }

    public function setCode($code)
    {
        $this->code = $code;
        return $this;
    }
}
