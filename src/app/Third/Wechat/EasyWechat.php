<?php

namespace App\Third\Wechat;

use App\Third\Wechat\Bag\Auth\Code2SessionBag;
use Core\Http\Client\HttpClient;

class EasyWechat
{
    public static function code2session($code)
    {
        $bag = new Code2SessionBag();
        $bag->setCode($code);

        $response = HttpClient::send($bag);

        $j = $response->getJson(true);
        if (empty($j) || $j->errcode != 0) {
            return null;
        }

        return $j;
    }
}
