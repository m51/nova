<?php

namespace App;

/**
 * 中间件配置, 中间件支持之前和之后
 * 执行顺序, 优先执行全局中间件, 然后在执行模块中间件
 */
class Kernel
{
    /**
     * 全局中间件
     */
    protected static $middleware = [
        \App\Middleware\HttpRequest::class
    ];

    /**
     * 指定模块中间件
     */
    protected static $middlewareGroups = [
        'api' => [],

        'admin' => [],
    ];

    /**
     * 这里定义的中间件需要显示配置才可用
     */
    protected static $routeMiddleware = [
        'auth' => \App\Middleware\Authenticate::class
    ];

    /**
     * 请求执行结束之后
     */
    protected static $afterMiddleware = [
        \App\Middleware\HttpRequestAfter::class
    ];
}
