<?php

namespace App\Middleware;

use Core\Http\Request;
use Core\Http\Response;
use Core\Log\Logger;
use Core\Route\MiddlewareInterface;
use G\User\AuthService;

class HttpRequestAfter implements MiddlewareInterface
{
    public function handle()
    {
        $r = Request::getInstance();

        $uid = AuthService::getInstance()->uid();
        $clientIP = $r->getClientIp();
        $userAgent = $r->userAgent();
        // use time float
        $utf = intval((microtime(true) - $r->server->get('REQUEST_TIME_FLOAT')) * 1000);

        // 只记录接口返回状态
        $resdata = Response::getInstance()->getData();
        unset($resdata['d']);

        Logger::info("[$clientIP][{$uid}]", ['userAgent' => $userAgent]);
        Logger::info("[$clientIP][{$utf}ms]", $resdata ?: []);
        return true;
    }
}
