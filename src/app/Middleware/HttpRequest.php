<?php

namespace App\Middleware;

use Core\Http\Request;
use Core\Log\Logger;
use Core\Route\MiddlewareInterface;

class HttpRequest implements MiddlewareInterface
{
    public function handle()
    {
        $r = Request::getInstance();

        $method     = $r->getMethod();
        $uri        = $r->getRequestUri();
        $clientIP   = $r->getClientIp();
        $method     = $r->getMethod();

        // 请求数据
        $input = $r->request->all();
        if ($r->getContentType() == 'json') {
            $input = $r->getJson();
        }

        Logger::info("[$clientIP][$method] {$uri}");
        Logger::info("[$clientIP][INPUT]", $input ?: []);
        return true;
    }
}
