<?php

namespace Core\Support;

/**
 * 创建级联树
 * @param array     $v              级联数组
 * @param mixin     $parentCode     顶级数据, 默认0
 * @param string    $nodeKey        作为上级关联的数据
 * @param string    $parentKey      表示关联数据的字段
 * @param int       $deep           级联关系获取深度
 * @param string    $childrenKey    级联关系关键词
 *
 * 使用示例:
 *
 *  $v = [
 *      ['id' => 100, 'pid' => 0],
 *      ['id' => 200, 'pid' => 0],
 *      ['id' => 300, 'pid' => 0],
 *      ['id' => 120, 'pid' => 100],
 *      ['id' => 121, 'pid' => 120],
 *      ['id' => 210, 'pid' => 200],
 *  ];
 *
 * $cascade = cascadeTree($v, 0, 'id', 'pid');
 */
class Cascade
{
    use Factory;

    /**
     * 关联父级字段
     */
    protected $parent_key = 'pid';

    /**
     * 主键字段
     */
    protected $node_key = 'id';

    /**
     * 子集字段
     */
    protected $child_key = 'child';

    public function parentKey(string $key)
    {
        $this->parent_key = $key;
        return $this;
    }

    public function nodeKey(string $key)
    {
        $this->parent_key = $key;
        return $this;
    }

    public function childrenKey(string $key)
    {
        $this->child_key = $key;
        return $this;
    }

    public function builder(array $v, $parentCode, $deep = 512)
    {
        $chilren = [];
        if ($deep) {
            $deep -= 1;
            foreach ($v as $item) {
                if ($item[$this->parent_key] == $parentCode) {
                    $item[$this->child_key] = $this->builder($v, $item[$this->node_key], $deep);
                    if (empty($item[$this->child_key])) {
                        unset($item[$this->child_key]);
                    }

                    $chilren[] = $item;
                }
            }
        }

        return $chilren;
    }

    public function generate(?array $v, $parentCode = 0)
    {
        if (is_null($v)) {
            return [];
        }

        return $this->builder($v, $parentCode);
    }

    /**
     * 创建级联树
     * @param array     $v              级联数组
     * @param mixin     $parentCode     顶级数据, 默认0
     * @param string    $nodeKey        作为上级关联的数据
     * @param string    $parentKey      表示关联数据的字段
     * @param int       $deep           级联关系获取深度
     * @param string    $childrenKey    级联关系关键词
     *
     * 使用示例:
     *
     *  $v = [
     *      ['id' => 100, 'pid' => 0],
     *      ['id' => 200, 'pid' => 0],
     *      ['id' => 300, 'pid' => 0],
     *      ['id' => 120, 'pid' => 100],
     *      ['id' => 121, 'pid' => 120],
     *      ['id' => 210, 'pid' => 200],
     *  ];
     *
     * $cascade = cascadeTree($v, 0, 'id', 'pid');
     */
    // public static function generate(array $v, $parentCode = 0, string $nodeKey = 'id', string $parentKey = 'pid', int $deep = 512, string $childrenKey = 'children')
    // {
    //     $chilren = [];
    //     if ($deep) {
    //         $deep -= 1;
    //         foreach ($v as $item) {
    //             if ($item[$parentKey] == $parentCode) {
    //                 $item[$childrenKey] = self::cascadeTree($v, $item[$nodeKey], $nodeKey, $parentKey, $deep, $childrenKey);
    //                 if (empty($item[$childrenKey])) {
    //                     unset($item[$childrenKey]);
    //                 }

    //                 $chilren[] = $item;
    //             }
    //         }
    //     }
    //     return $chilren;
    // }
}
