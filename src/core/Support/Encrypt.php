<?php

namespace Core\Support;

class Encrypt
{
    public static function sign($body, $slat, $secret)
    {
        return hash_hmac('haval160,4', crypt(base64_decode($body), $slat), $secret);
    }

    /**
     * 解码密码
     */
    // public static function decodePassword($password)
    // {
    //     $a = base64_decode($password);
    // }

    /**
     * user_encryptPassword
     */
    public static function passwordHash($pass)
    {
        return convert_uuencode(password_hash($pass, PASSWORD_ARGON2ID));
    }

    /**
     * user_authentication
     */
    public static function authentication($pass, $hash)
    {
        if ($pass && $hash) {
            if (password_verify($pass, convert_uudecode($hash))) {
                return true;
            }
        }
    }
}
