<?php

namespace Core\Support;

use Illuminate\Support\Arr as SupportArr;

class Arr extends SupportArr
{
    /**
     * 检查字符串是否在数组中, 如果存在返回该字符串, 不存在返回默认值
     */
    public static function getExist($str, array $data, $default = null)
    {
        return in_array($str, $data) ? $str : $default;
    }
}
