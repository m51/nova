<?php

namespace Core\Support;

use Attribute;
use Core\Http\Request;

#[Attribute(Attribute::TARGET_METHOD | Attribute::IS_REPEATABLE)]
class Validate
{
    public function __construct($name, $message)
    {
        Request::getInstance()->setValidation($name, $message);
    }
}
