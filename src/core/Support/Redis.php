<?php

namespace Core\Support;

use Illuminate\Redis\RedisManager;
use Illuminate\Support\Arr;

/**
 * Redis::store(DXE_SESSION)->get($token)
 * Redis::store(DXE_SESSION)->del($login->token)
 */
class Redis
{
    protected static $redisManager;

    public static function loadConfig(array $option)
    {
        $config = Arr::get($option, 'redis');

        if (empty($config) != true) {
            self::$redisManager = new RedisManager(null, Arr::pull($config, 'client', 'phpredis'), $config);
        }
    }

    public static function connection($name = null)
    {
        return self::$redisManager->connection($name);
    }

    public static function store($name = null)
    {
        return self::$redisManager->connection($name);
    }

    // put 指定的 key 设置值及其过期时间。如果 key 已经存在，将会替换旧的值。
    // public static function put(string $key, $data, int $expirt)
    // {
    //     if (is_array($data)) {
    //         return self::setex($key, $expirt, "a:" . json_encode($data));
    //     }

    //     if (is_object($data)) {
    //         return self::setex($key, $expirt, "s:" . serialize($data));
    //     }

    //     return self::setex($key, $expirt, $data);
    // }

    // public static function get(string $key)
    // {
    //     $data = self::get
    // }

    public static function __callStatic($method, $parameters)
    {
        return self::$redisManager->connection()->{$method}(...$parameters);
    }
}
