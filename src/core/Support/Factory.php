<?php

namespace Core\Support;

trait Factory
{
    public static function factory()
    {
        return new self;
    }
}