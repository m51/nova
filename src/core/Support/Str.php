<?php

namespace Core\Support;

use Illuminate\Support\Str as SupportStr;
use Ramsey\Uuid\Uuid;

class Str extends SupportStr
{
    /**
     * 获取uuid4
     *  ->getHex()      获取Hex编码
     */
    public static function uuid4()
    {
        return Uuid::uuid4();
    }
}
