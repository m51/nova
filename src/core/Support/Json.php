<?php

namespace Core\Support;

class Json
{
    public static function encode($data, int $flags = 0)
    {
        return json_encode($data, $flags);
    }

    public static function decode($data, $associative)
    {
        return json_decode($data, $associative);
    }
}
