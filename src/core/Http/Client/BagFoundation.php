<?php

namespace Core\Http\Client;

abstract class BagFoundation implements BagInterface
{
    protected $headers;

    public function header()
    {
        return $this->headers;
    }

    public function body()
    {
        return [];
    }

    public function query()
    {
        return [];
    }

    /**
     * set header
     */
    public function setHeader(string $key, $val)
    {
        $this->headers[$key] = $val;
    }
}