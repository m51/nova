<?php

namespace Core\Http\Client;

interface BagInterface
{
    const METHOD_HEAD = 'HEAD';
    const METHOD_GET = 'GET';
    const METHOD_POST = 'POST';
    const METHOD_PUT = 'PUT';
    const METHOD_PATCH = 'PATCH';
    const METHOD_DELETE = 'DELETE';
    const METHOD_PURGE = 'PURGE';
    const METHOD_OPTIONS = 'OPTIONS';
    const METHOD_TRACE = 'TRACE';
    const METHOD_CONNECT = 'CONNECT';

    // 返回url地址
    public function url();

    // 请求方法
    public function method();

    // 请求Header
    public function header();

    // Post参数
    public function body();

    // Get参数
    public function query();
}