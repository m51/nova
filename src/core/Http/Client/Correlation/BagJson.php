<?php

namespace Core\Http\Client\Correlation;

trait BagJson
{
    public function header()
    {
        $this->setHeader('Content-type', 'application/json; charset=UTF-8');

        return parent::header();
    }
}
