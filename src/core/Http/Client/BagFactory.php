<?php

namespace Core\Http\Client;

/**
 * 待验证
 */
class BagFactory extends BagFoundation
{
    protected $url;
    protected $body;
    protected $query;
    protected $method;

    public static function create()
    {
        return new static();
    }

    /**
     * 这是一个Get请求
     */
    public function getRequest(string $url, array $query = [])
    {
        $this->method = self::METHOD_GET;
        $this->url = $url;
        $this->query = $query;
    }

    /**
     * 这是一个Post请求
     */
    public function postRequest(string $url, array $body = [])
    {
        $this->method = self::METHOD_POST;
        $this->url = $url;
        $this->body = $body;
    }

    public function url()
    {
        return $this->url;
    }

    public function method()
    {
        return $this->method;
    }

    public function body()
    {
        return $this->body;
    }

    public function query()
    {
        return $this->query;
    }
}
