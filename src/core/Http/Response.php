<?php

namespace Core\Http;

use Core\Support\SingleTon;
use Symfony\Component\HttpFoundation\Response as HttpFoundationResponse;

class Response extends HttpFoundationResponse
{
    use SingleTon;

    protected $data;

    /**
     * 返回成功结果
     */
    public function success($data = null, array $attr = [])
    {
        $this->data = $attr;

        $this->data['d'] = $data;
        $this->data['e'] = 0;
        $this->data['m'] = 'success';

        $this->headers->set('Content-Type', 'application/json; charset=utf-8');
        $this->setContent(json_encode($this->data));

        return $this;
    }

    /**
     * 返回失败结果
     */
    public function fail($msg, $errno)
    {
        $this->data['e'] = $errno;
        $this->data['m'] = $msg;

        $this->headers->set('Content-Type', 'application/json; charset=utf-8');
        $this->setContent(json_encode($this->data));

        return $this;
    }

    /**
     * Get Data
     */
    public function getData()
    {
        return $this->data;
    }
}
