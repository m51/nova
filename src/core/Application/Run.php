<?php

namespace Core\Application;

use Symfony\Component\DependencyInjection\ContainerBuilder;

class Run
{
    /**
     * 运行配置文件
     */
    protected $runConfig;

    /**
     * 路由调度器
     */
    protected $dispatcher;

    /** 容器服务 */
    protected $container;

    public function __construct($config)
    {
        $this->runConfig = $config;

        // $this->container = new ContainerBuilder();
        // $this->container->register('mailer', Mailer::class);

        // 创建调度器
        $this->dispatcher = new Dispatcher($this->runConfig['routes'] ?: []);
    }

    /**
     * 路由调度
     * @param $method       string      Http Method
     * @param $uri          string      Http Uri
     */
    public function dispatch($method, $uri)
    {
        $this->dispatcher
            ->setRequestMethod($method)
            ->setRequestUri($uri)
            ->dispatch();
    }
}
