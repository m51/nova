<?php

namespace Core\Application;

use Core\Http\Request;
use Core\Http\Response;

/**
 * 犯懒了, 先放这里吧
 */
interface ControllerInterface {

};

class Controller implements ControllerInterface
{
    protected $request;

    protected $response;

    public function __construct()
    {
        $this->request = Request::getInstance();
        $this->response = Response::getInstance();
    }

    // Gets a "parameter" value from any bag.
    // public function getInput(string $key, mixed $default = null): mixed
    // {
    //     return $this->request->get($key, $default);
    // }

    // 获取Param/FormData输入数据
    public function getInputAll(): mixed
    {
        $data = $this->request->query->all();

        // 校验数据
        $this->request->validated(null, $data ?: []);

        return $data;
    }

    /**
     * 获取Json输入数据
     */
    public function getInputJson(string $key = null, mixed $default = null): mixed
    {
        $data = $this->request->getJson($key, $default);

        // 校验数据
        $this->request->validated($key, $data ?: []);

        return $data;
    }

    public function success($data = null, array $attr = [])
    {
        return $this->response->success($data, $attr);
    }

    public function fail($errmsg)
    {
        $errno = $this->request->getRequestId();

        return $this->response->fail($errmsg, $errno);
    }
}
