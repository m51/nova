<?php

namespace Core\Database;

use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Support\Arr;

class Configuration
{
    public static function loadConfig(array $option)
    {
        $capsule = new Capsule();

        $default = Arr::get($option, 'default');
        $connections = Arr::get($option, 'connections');

        // add connections
        foreach ($connections as $name => $connectionsParams) {
            if ($name == $default) {
                $name = 'default';
            }

            $capsule->addConnection($connectionsParams, $name);
        }

        // $capsule->setEventDispatcher(new Dispatcher(new Container));
        $capsule->setAsGlobal();
        $capsule->bootEloquent();
    }
}
