<?php

namespace Core\Database\Criteria;

/**
 * Query Criteria
 */
trait QueryCriteria
{
    protected $query;

    // 必须实现newQuery
    // public function newQuery()
    // {
    //     $this->query = CartModel::query();
    //     return $this;
    // }

    // 从表中检索所有行
    public function get()
    {
        return $this->query->get();
    }

    // 从表中检索单行或单列
    public function first()
    {
        return $this->query->first();
    }

    // 通过 id 字段值获取单行数据
    public function find($id)
    {
        return $this->query->find($id);
    }

    // 分页
    public function paginate()
    {
        return $this->query->paginate();
    }

    // 删除
    public function delete()
    {
        return $this->query->delete();
    }

    // 获取某一列的值
    // public function pluck()
    // {
    //     return $this->query->pluck();
    // }

    // 分块结果
    // public function chunk()
    // {
    //     # code...
    // }
}
