<?php

namespace Core\Config;

use Illuminate\Support\Arr;

/**
 * 配置文件操作
 */
class Config
{
    protected static $data;

    public static function load($path)
    {
        self::$data = ConfigLoader::loader($path);
    }

    /**
     * get value
     */
    public static function get(string $key, $default = null)
    {
        return Arr::get(self::$data, $key, $default);
    }

    /**
     * set value
     */
    public static function set(string $key, $val)
    {
        Arr::set(self::$data, $key, $val);
    }

    /**
     * get all
     */
    public static function all()
    {
        return self::$data;
    }

    /**
     * has key
     */
    public static function has(string $key)
    {
        return Arr::has(self::$data, $key);
    }
}

