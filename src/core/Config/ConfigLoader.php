<?php

namespace Core\Config;

/**
 * 配置文件装载
 */
class ConfigLoader
{
    public static function loader($path)
    {
        $data = [];
        if (is_dir($path)) {
            $path = glob($path . '/*.php');

            foreach ($path as $file) {
                $filename = pathinfo($file)['filename'];

                $data[$filename] = require $file;
            }
        }

        return $data;
    }
}

