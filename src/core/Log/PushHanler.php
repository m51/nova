<?php

namespace Core\Log;

use Core\Config\Config;
use Monolog\Handler\StreamHandler;

trait PushHanler
{
    /**
     * 获取日志存储文件地址
     */
    protected function getPushFile($stack)
    {
        $file = preg_replace_callback('/\{.*\}/', function ($matches) {
            return trim(date($matches[0], time()), '{}');
        }, $stack['file'], 1);

        return $file;
    }

    /**
     * 清理旧的日志文件
     */
    protected function cleanUp($stack)
    {
        # code...
    }

    /**
     * push handler
     */
    public function pushHandler()
    {
        $stack = Config::get('logging.stack');

        $file = $this->getPushFile($stack);
        $this->_logger->pushHandler(new StreamHandler($file));
    }
}