<?php

namespace Core\Log;

use Core\Http\Request;
use Core\Support\SingleTon;
use Monolog\Logger as MonologLogger;

class Logger
{
    use SingleTon;
    use PushHanler;

    protected $_logger;

    public function __construct()
    {
        $requestId = Request::getInstance()->getRequestId();
        $this->_logger = new MonologLogger($requestId);

        $this->pushHandler();
    }

    public static function __callStatic($method, $parameters)
    {
        return self::getInstance()->_logger->{$method}(...$parameters);
    }

    // /**
    //  * Adds a log record at the DEBUG level.
    //  */
    // public function debug(string $message, array $context = []): void
    // {
    //     $this->_logger->debug($message, $context);
    // }

    // /**
    //  * Adds a log record at the INFO level.
    //  */
    // public function info(string $message, array $context = []): void
    // {
    //     $this->_logger->info($message, $context);

    //     // back trace
    //     // $trace = debug_backtrace(DEBUG_BACKTRACE_PROVIDE_OBJECT, 1);
    //     // foreach ($backtrace as $trace) {
    //     //     // $this->_logger->info('> ', $trace);
    //     //     // $this->_logger->info("file {file}, {{line}}", $trace);
    //     //     $this->_logger->info("file " . $trace['file'] . ', ' . $trace['line']);
    //     // }

    //     // $xx = "file {$trace[0]['file']}, {$trace[0]['line']}";
    //     // $this->_logger->info($xx . $message , $context);
    // }

    // /**
    //  * Adds a log record at the NOTICE level.
    //  */
    // public function notice(string $message, array $context = []): void
    // {
    //     $this->_logger->notice($message, $context);
    // }

    // /**
    //  * Adds a log record at the WARNING level.
    //  */
    // public function warning(string $message, array $context = []): void
    // {
    //     $this->_logger->warning($message, $context);
    // }

    // /**
    //  * Adds a log record at the ERROR level.
    //  */
    // public function error(string $message, array $context = []): void
    // {
    //     $this->_logger->error($message, $context);
    // }

    // /**
    //  * Adds a log record at the CRITICAL level.
    //  */
    // public function critical(string $message, array $context = []): void
    // {
    //     $this->_logger->critical($message, $context);
    // }

    // /**
    //  * Adds a log record at the ALERT level.
    //  */
    // public function alert(string $message, array $context = []): void
    // {
    //     $this->_logger->alert($message, $context);
    // }

    // /**
    //  * Adds a log record at the EMERGENCY level.
    //  */
    // public function emergency(string $message, array $context = []): void
    // {
    //     $this->_logger->emergency($message, $context);
    // }
}
