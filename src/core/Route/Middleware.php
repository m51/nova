<?php

namespace Core\Route;

interface Middleware
{
    public function getData();
    public function setData($data);

    public function setCurrent(array $data);
    public function mergeCurrent();
}
