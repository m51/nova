<?php

namespace Core\Route;

interface MiddlewareInterface
{
    public function handle();
}
