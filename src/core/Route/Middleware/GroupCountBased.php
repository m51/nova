<?php

namespace Core\Route\Middleware;

use Core\Route\Middleware;

class GroupCountBased implements Middleware
{
    /**
     * 当前路由中间件数据(只包含当前)
     */
    protected $current;

    /**
     * 当前路由中间件数据(包含父级)
     */
    protected $deep = [];

    public function setCurrent(array $data)
    {
        $this->current = $data;
    }

    /**
     * 合并当前中间件配置
     */
    public function mergeCurrent()
    {
        if (!empty($this->current)) {
            $this->old = $this->deep;
            $this->deep = array_merge($this->deep, $this->current);
        }

        $this->current = [];
    }

    public function setData($data)
    {
        $this->deep = $data;
    }

    public function getData()
    {
        return $this->deep;
    }
}
