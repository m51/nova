<?php

use Elastic\Elasticsearch\ClientBuilder;

class ClientManager
{
    protected static $connectPool;

    public static function connect($name = 'default')
    {
        if (!array_key_exists($name, self::$connectPool)) {
            self::$connectPool[$name] = self::createConnect($name);
        }

        return self::$connectPool[$name];
    }

    // public function connect($name = 'default')
    // {
    //     // return $this->createConnect($config);
    // }

    /**
     * Create Connect
     */
    public function createConnect()
    {
        $create = ClientBuilder::create();

        $create->setHosts(['https://localhost:9200']);

        // 使用密码
        // if (true) {
        //     $create->setBasicAuthentication('elastic', 'password copied during Elasticsearch start');
        // }
        // 使用证书加密
        // if (true) {
        //     $create->setCABundle('path/to/http_ca.crt');
        // }

        $client = $create->build();

        return $client;
    }

    /**
     * Create Cloud Connect
     */
    public function createCloudConnect()
    {
        # code...
    }
}
