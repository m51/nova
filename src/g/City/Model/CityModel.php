<?php

namespace G\City\Model;

use Core\Database\Eloquent\Model;

class CityModel extends Model
{
    protected $table = 'g_city';
    protected $dateFormat = 'U';

    public function getPinyinAttribute($val)
    {
        return json_decode($val);
    }
}
