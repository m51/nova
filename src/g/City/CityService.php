<?php

namespace G\City;

use Core\Support\Cascade;
use Core\Support\Redis;
use Core\Support\SingleTon;
use G\City\Model\CityModel;

class CityService
{
    use SingleTon;

    public function getCascade()
    {
        $data = $this->getCache();
        if (empty($data)) {
            $data = $this->buildCache();
        }

        return $data;
    }


    public function getCache()
    {
        return false;
    }

    /**
     * 构建缓存
     */
    public function buildCache()
    {
        $list = CityModel::all();
        $cascade = Cascade::factory()->generate($list->toArray());

        return $cascade;
    }
}
