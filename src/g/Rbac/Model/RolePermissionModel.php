<?php

namespace G\Rbac\Model;

use Core\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * 角色权限配置扩展属性
 * extend: {
 *      // 是否有所有数据权限
 *      all: 1,
 *      // 权限范围, 还没理顺--待定
 *      range: {
 *          city: [12, 32]          // 城市下的数据权限
 *          deparment: [12]         // 部门权限
 *      }
 * }
 */
class RolePermissionModel extends Model
{
    use SoftDeletes;

    protected $table = 'g_role_permission';
    protected $dateFormat = 'U';

    const CREATED_AT = 'ct';
    const UPDATED_AT = 'ut';
    const DELETED_AT = 'dt';

    // 可批量赋值的属性 insert|update
    protected $fillable = [
        'role_id',
        'permission_key',
        'extend',
    ];

    // 不可以批量赋值的属性
    protected $guarded = [
        'creator'
    ];

    protected $hidden = [
        'dt',
    ];

    protected $casts = [
        'extend' => 'array'
    ];
}
