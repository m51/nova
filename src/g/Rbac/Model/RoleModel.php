<?php

namespace G\Rbac\Model;

use Core\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RoleModel extends Model
{
    use SoftDeletes;

    protected $table = 'g_role';
    protected $dateFormat = 'U';

    const CREATED_AT = 'ct';
    const UPDATED_AT = 'ut';
    const DELETED_AT = 'dt';

    protected $hidden = [
        'dt',
    ];
}
