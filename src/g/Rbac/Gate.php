<?php

namespace G\Rbac;

use Core\Log\Logger;
use Core\Support\Arr;
use Core\Support\SingleTon;
use G\Rbac\Model\RolePermissionModel;
use G\Rbac\Model\UserRoleModel;
use G\User\AuthService;
use G\User\Model\AdminModel;

class Gate
{
    use SingleTon;

    /**
     * 是否有权限
     */
    public function can(string $key, ...$params)
    {
        // 获取用户角色
        // $roles =

        // 是否是超管

        // 是否有权限

        // 检查权限范围
    }

    /**
     * @param string    $key
     * @param array     $option
     *  creator 需要匹配创建人
     */
    public static function not(string $key, array $option)
    {
        $user = AuthService::getInstance()->user();

        // 匹配创建人
        if (array_key_exists('creator', $option)) {
            if ($user->id == $option['creator']) {
                return false;
            }
        }

        // 是否是超级管理员
        $admin = AdminModel::where('uid', $user->id)->first();
        if ($admin && $admin->isRoot()) {
            return false;
        }

        // 管理员才有权限配置
        $rp = self::getRolePermission($user->id, $key);
        if ($rp && $admin) {
            // 拥有所有数据权限
            $ext_all = Arr::get($rp, 'extend.all');
            if ($ext_all) {
                return false;
            }

            // 指定数据权限
            // $ext_range = Arr::get($rp, 'extend.range');
            // if (in_array()) {
            //     // todo
            // }
        }

        // 没有权限
        Logger::warning("[GATE] 没有权限: ", ['perm' => $key, 'option' => $option]);
        return true;
    }

    public static function getRolePermission($uid, string $key)
    {
        $permission = RolePermissionModel::addSelect(['role_id' => UserRoleModel::select('id')->where('uid', $uid)])
            ->where('permission_key', $key)
            ->first();

        return $permission;
    }
}
