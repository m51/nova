<?php

namespace G\User\Model;

use Core\Database\Eloquent\Model;

class LoginModel extends Model
{
    protected $table = 'g_login';
    protected $dateFormat = 'U';

    const CREATED_AT = 'ct';
    const UPDATED_AT = 'ut';

    const DEVICE_APP    = 1;        // 使用APP登录
    const DEVICE_WEB    = 3;        // 使用网页登录
    const DEVICE_WECHAT = 7;        // 在微信中登录

    protected $fillable = [
        'uid',
        'token',
        'device',
        'expire',
        'ipaddr',
        'last',
        'ua'
    ];

    public static function whereUid($uid)
    {
        return self::where('uid', $uid);
    }

    public static function whereDevice(int $device)
    {
        return self::where('device', $device);
    }
}
