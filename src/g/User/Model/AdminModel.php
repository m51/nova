<?php

namespace G\User\Model;

use Core\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AdminModel extends Model
{
    use SoftDeletes;

    protected $table = 'g_admin';
    protected $dateFormat = 'U';

    const CREATED_AT = 'ct';
    const UPDATED_AT = 'ut';
    const DELETED_AT = 'dt';

    const TYPE_ROOT    = 1;      // 超级管理员
    const TYPE_DEFAUTL = 2;      // 普通管理员

    // 可批量赋值的属性
    protected $fillable = [
        'uid',
        'type',
    ];

    protected $hidden = [
        'dt',
    ];

    protected $casts = [
        'ct' => 'datetime:Y-m-d H:i:s',
        'ut' => 'datetime:Y-m-d H:i:s',
    ];

    public function isRoot()
    {
        if ($this->type == self::TYPE_ROOT) {
            return true;
        }

        return false;
    }
}
